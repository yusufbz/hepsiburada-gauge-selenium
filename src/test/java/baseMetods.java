import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class baseMetods {
    @BeforeScenario
    public void hazirlik() {
        System.out.println("Senaryo Hazirlaniyor");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    static WebDriver driver;
    static String expectedTitle = "Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com";
    static String expectedLoginTitle = "Üye Giriş Sayfası & Üye Ol - Hepsiburada";
    static String actualTitle;

    public static void getUrl(){
        driver.get("https://www.hepsiburada.com");
    }

    public static String getTitle(){
        return driver.getTitle();
    }

    public static WebElement findElement (By by){
        return driver.findElement(by);
    }

    public static List<WebElement> findElements(By by){
        return driver.findElements(by);
    }

    public static void clickElement(By by){
        findElement(by).click();
    }

    public static boolean displayElement(By by) {
        try {
            return findElement(by).isDisplayed();

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void sendElement(By by, String text){
        findElement(by).sendKeys(text);
    }

    public static void performElement(By by){
        Actions action = new Actions(driver);
        WebElement element = findElement(by);
        action.moveToElement(element).perform();
        action.moveToElement(findElement(by)).click().build().perform();

    }

    @AfterScenario
    public void bitir() {
        driver.quit();
        System.out.println("Senaryo Tamamlandi");
    }
}
