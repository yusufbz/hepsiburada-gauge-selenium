import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import static org.junit.Assert.fail;

public class Anasayfa extends baseMetods {


    @Step("Anasayfaya Git")
    public void anaSayfayaGit() {
        getUrl();
        actualTitle = getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
        System.out.println("Ana Sayfa Yuklendi");
    }


    @Step("Buton Kontrol Et")
    public void butonKontrolAnasayfa() {

        boolean butonIcon = displayElement(By.id("shoppingCart"));
        if (butonIcon) {
            System.out.println("Buton Mevcut, Ana Sayfadasiniz.");
        } else {
            System.out.println("Buton Mevcut Degil , Ana Sayfada degilsiniz");
            fail();
        }
    }


    @Step("Giris Sayfasina Git")
    public void girisSayfasi() throws InterruptedException {
        performElement(By.id("myAccount"));
        Thread.sleep(2000);
        performElement(By.id("login"));
        Thread.sleep(4000);
        System.out.println("Giris Sayfasina Yonlenildi");

    }


    @Step("Giris Sayfasi Buton Kontrol")
    public void butonKontrolGiris() {
        boolean butonIcon = displayElement(By.id("btnLogin"));
        if (butonIcon) {
            System.out.println("Buton Mevcut, Giris Sayfasindasiniz.");
        } else {
            System.out.println("Buton Mevcut Degil , Giris Sayfasinda degilsiniz");
            fail("Test Basarisiz");
        }
    }


    @Step("<email> ve <sifre> ile sisteme giris")
    public void girisBilgileri(String email, String sifre) {
        sendElement(By.id("txtUserName"), email); //mail adresinizi users.csv'ye ekleyiniz
        sendElement(By.id("txtPassword"), sifre); //sifrenizi users.csv'ye ekleyiniz
    }



    @Step("Giris Butonuna tıkla")
    public void clickedLogin() throws InterruptedException {
        clickElement(By.id("btnLogin"));
        Thread.sleep(4000);
        String loginTitle = getTitle();
        if(loginTitle.equalsIgnoreCase(expectedLoginTitle)) {
            System.out.println("Uye Girisi Basarisiz");
            Assert.fail();

        }else if(loginTitle.equalsIgnoreCase(expectedTitle)){
            System.out.println("Giris Basarili");
        }
        else {
            System.out.println("Sayfa" + loginTitle + " a yonlendirildi. ");
        }
    }


    @Step("<isimSoyisim> Giris Dogrulugu Kontrol")
    public void girisKontrol(String isimSoyisim) {
        List<WebElement> actualUsername = findElements(By.xpath("//*[contains(text(),'" + isimSoyisim + "')]")); // isim soyisminizi users.csv dosyasına ekleyiniz
        if (actualUsername.size() > 0) {
            System.out.println(" " + isimSoyisim + " kullanicisi ile giris kontrolu basarili. ");
        } else {
            System.out.println(" " + isimSoyisim + " kullanicisi ile giris kontrolu basarisiz.");
            fail("Test Basarisiz");

        }
    }

}

